<?php

/**
 * @file
 * Rules integration for Node type flag module.
 */

/**
 * Implements hook_rules_action_info_alter().
 */
function node_type_flag_rules_action_info_alter(&$info) {
  $actions = array(
    'flag_flagnode_type',
    'flag_unflagnode_type',
    'flag_fetch_users_node_type',
  );
  // Since node_type is not an entity we replace it by node.
  foreach ($actions as $action) {
    if (isset($info[$action]['parameter']['node_type'])) {
      $info[$action]['parameter']['node_type']['type'] = 'node';
    }
  }
  // We need custom action callbacks for fetching users and node types.
  $info['flag_fetch_users_node_type']['base'] = 'node_type_flag_fetch_users_node_type';
  $info['flag_fetch_node_type_by_user']['base'] = 'node_type_flag_fetch_node_type_by_user';

  // Add labels for Rules admin UI.
  $info['flag_fetch_node_type_by_user']['label'] = t('Fetch Node type flagged by user');
  $info['flag_flagnode_type']['label'] = t('Flag a Node type');
  $info['flag_unflagnode_type']['label'] = t('Unflag a Node type');
  $info['flag_fetch_users_node_type']['label'] = t('Fetch users who have flagged a Node type');
}
