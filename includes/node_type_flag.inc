<?php

/**
 * @file
 * Contains the node_type_flag class.
 */

/**
 * A class to flag node types.
 */
class node_type_flag extends flag_flag {

  /**
   * Declares the options this flag supports, and their default values.
   *
   * Derived classes should want to override this.
   */
  function options() {
    $options = parent::options();
    if (module_exists('og')) {
      $options['group'] = FALSE;
    }
    $options += array(
      // Output the flag in the entity links.
      // This is empty for now and will get overriden for different
      // entities.
      // @see hook_entity_view().
      'show_in_links' => array(),
      // Output the flag as individual pseudofields.
      'show_as_field' => FALSE,
      // Add a checkbox for the flag in the entity form.
      // @see hook_field_attach_form().
      'show_on_form' => FALSE,
      'show_contextual_link' => FALSE,
    );
    return $options;
  }
  /**
   * Provides a form for setting options.
   *
   * Derived classes should want to override this.
   */
  function options_form(&$form) {
    $bundles = array();
    $entity_info = entity_get_info('node');
    foreach ($entity_info['bundles'] as $bundle_key => $bundle) {
      $bundles[$bundle_key] = check_plain($bundle['label']);
    }
    $form['access']['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Bundles'),
      '#options' => $bundles,
      '#description' => t('Select the node types that this flag may be used on. Leave blank to allow on all node types.'),
      '#default_value' => $this->types,
    );
    if (module_exists('og')) {
      $form['group'] = array(
        '#type' => 'checkbox',
        '#title' => t('Flag by group'),
        '#description' => t('Flagging the node type only in the current OG group.'),
        '#default_value' => $this->group,
        '#weight' => 5,
      );
    }
    $options = array();
    $defaults = array();
    foreach ($entity_info['view modes'] as $name => $view_mode) {
      $options[$name] = t('Display on @name view mode', array('@name' => $view_mode['label']));
      $defaults[$name] = !empty($this->show_in_links[$name]) ? $name : 0;
    }

    $form['display']['show_in_links'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Display in entity links'),
      '#description' => t('Show the flag link with the other links on the entity.'),
      '#options' => $options,
      '#default_value' => $defaults,
    );

    $form['display']['show_as_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display link as field'),
      '#description' => t('Show the flag link as a pseudofield, which can be ordered among other entity elements in the "Manage display" settings for the entity type.'),
      '#default_value' => isset($this->show_as_field) ? $this->show_as_field : TRUE,
    );
    if (empty($entity_info['fieldable'])) {
      $form['display']['show_as_field']['#disabled'] = TRUE;
      $form['display']['show_as_field']['#description'] = t("This entity type is not fieldable.");
    }

    $form['display']['show_on_form'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display checkbox on entity edit form'),
      '#default_value' => $this->show_on_form,
      '#weight' => 5,
    );
  }

  /**
   * Return a node type object this flag works with.
   */
  function _load_entity($entity_id) {
    if (is_numeric($entity_id)) {
      $node = node_load($entity_id);
      $node_type = new stdClass();
      $node_type->entity_id = $node->type;
      if (module_exists('og') && !empty($this->group)) {
        $node_group = og_get_entity_groups('node', $entity_id);
        // Let's take the first OG group this node belongs to.
        if (!empty($node_group)) {
          $node_group = reset($node_group);
          $node_group = reset($node_group);
          $node_type->group = $node_group;
        }
        // If this is supposed to be an OG node type flag and the entity
        // still does not belong to any group there's some error.
        else {
          return NULL;
        }
      }
      return $node_type;
    }
    return NULL;
  }

  /**
   * Returns TRUE if the flag applies to the given entity.
   */
  function applies_to_entity($entity) {
    $entity_info = entity_get_info('node');
    if (empty($this->types) || empty($entity_info['entity keys']['bundle']) || in_array($entity->type, $this->types)) {
      if (!empty($this->group)) {
        if (!module_exists('og')) {
          return FALSE;
        }
        $node_group = og_get_entity_groups('node', $entity);
        if (empty($node_group)) {
          return FALSE;
        }
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns TRUE if the flag applies to the entity with the given ID.
   *
   * This is a convenience method that simply loads the object and calls
   * applies_to_entity(). If you already have the object, don't call
   * this function: call applies_to_entity() directly.
   */
  function applies_to_entity_id($entity_id) {
    return $this->applies_to_entity(node_load($entity_id));
  }

  /**
   * Implements access_multiple() implemented by each child class.
   *
   * @return
   *  An array keyed by entity ids, whose values represent the access to the
   *  corresponding entity. The access value may be FALSE if access should be
   *  denied, or NULL (or not set) if there is no restriction to  be made. It
   *  should NOT be TRUE.
   */
  function type_access_multiple($entity_ids, $account) {
    $access = array();

    // If all subtypes are allowed, we have nothing to say here.
    if (empty($this->types)) {
      if (empty($this->group)) {
        return $access;
      }
      else {
        foreach ($entity_ids as $entity_id) {
          if (!module_exists('og')) {
            $access[$entity_id] = FALSE;
          }
          $node_group = og_get_entity_groups('node', $entity_id);
          if (empty($node_group)) {
            $access[$entity_id] = FALSE;
          }
        }
      }
    }
    else {
      // Ensure that only flaggable node types are granted access. This avoids a
      // node_load() on every type, usually done by applies_to_entity_id().
      $result = db_select('node', 'n')->fields('n', array('nid'))
        ->condition('nid', array_keys($entity_ids), 'IN')
        ->condition('type', $this->types, 'NOT IN')
        ->execute();
      foreach ($result as $row) {
        $access[$row->nid] = FALSE;
      }
      // Check if the node belongs to any group.
      $result = db_select('node', 'n')->fields('n', array('nid'))
        ->condition('nid', array_keys($entity_ids), 'IN')
        ->condition('type', $this->types)
        ->execute();
      foreach ($result as $row) {
        $node_group = og_get_entity_groups('node', $row->nid);
        if (empty($node_group)) {
          $access[$row->nid] = FALSE;
        }
      }
    }
    return $access;
  }

  /**
   * Given a node, returns its nid.
   */
  function get_entity_id($entity) {
    return $entity->nid;
  }

  /**
   * Utility function: Checks whether a flag applies to a certain type, and
   * possibly subtype, of entity.
   *
   * @param $entity_type
   *   The type of entity being checked, such as "node".
   * @param $content_subtype
   *   The subtype being checked. For entities this will be the bundle name (the
   *   node type in the case of nodes).
   *
   * @return
   *   TRUE if the flag is enabled for this type and subtype.
   */
  function access_entity_enabled($entity_type, $content_subtype = NULL) {
   $entity_type_matches = ('node' == $entity_type);
   $sub_type_matches = FALSE;
   if (!isset($content_subtype) || !count($this->types)) {
     // Subtype automatically matches if we're not asked about it,
     // or if the flag applies to all subtypes.
     $sub_type_matches = TRUE;
   }
   else {
     $sub_type_matches = in_array($content_subtype, $this->types);
   }
   return $entity_type_matches && $sub_type_matches;
 }

  /**
   * Determine whether the flag should show a flag link in entity links.
   *
   * Derived classes are likely to implement this.
   *
   * @param $view_mode
   *   The view mode of the entity being displayed.
   *
   * @return
   *   A boolean indicating whether the flag link is to be shown in entity
   *   links.
   */
  function shows_in_entity_links($view_mode) {
    // Check for settings for the given view mode.
    if (isset($this->show_in_links[$view_mode])) {
      return (bool) $this->show_in_links[$view_mode];
    }
    return FALSE;
  }

  /**
   * Construct a new, empty flagging entity object.
   *
   * @param mixed $entity_id
   *   The unique identifier of the object being flagged.
   * @param int $uid
   *   (optional) The user id of the user doing the flagging.
   * @param mixed $sid
   *   (optional) The user SID (provided by Session API) who is doing the
   *   flagging. The SID is 0 for logged in users.
   *
   * @return stdClass
   *   The returned object has at least the 'flag_name' property set, which
   *   enables Field API to figure out the bundle, but it's your responsibility
   *   to eventually populate 'entity_id' and 'flagging_id'.
   */
  function new_flagging($entity_id = NULL, $uid = NULL, $sid = NULL) {
    $properties = _node_type_flag_get_node_type_and_gid($entity_id);
    $gid = $this->group ? $properties['gid'] : 0;
    return (object) array(
      'flagging_id' => NULL,
      'flag_name' => $this->name,
      'entity_id' => $properties['node_type'],
      'gid' => $gid,
      'uid' => $uid,
      'sid' => $sid,
    );
  }

  /**
   * Determines if a certain user has flagged this object.
   *
   * Thanks to using a cache, inquiring several different flags about the same
   * item results in only one SQL query.
   *
   * @param $uid
   *   (optional) The user ID whose flags we're checking. If none given, the
   *   current user will be used.
   *
   * @return
   *   TRUE if the object is flagged, FALSE otherwise.
   */
  function is_flagged($entity_id, $uid = NULL, $sid = NULL) {
    return (bool) $this->get_flagging_record($entity_id, $uid, $sid);
  }

  /**
   * Returns the flagging record.
   *
   * This method returns the "flagging record": the {flagging} record that
   * exists for each flagged item (for a certain user). If the item isn't
   * flagged, returns NULL. This method could be useful, for example, when you
   * want to find out the 'flagging_id' or 'timestamp' values.
   *
   * Thanks to using a cache, inquiring several different flags about the same
   * item results in only one SQL query.
   *
   * Parameters are the same as is_flagged()'s.
   */
  function get_flagging_record($entity_id, $uid = NULL, $sid = NULL) {
    $uid = $this->global ? 0 : (!isset($uid) ? $GLOBALS['user']->uid : $uid);
    $sid = $this->global ? 0 : (!isset($sid) ? flag_get_sid($uid) : $sid);

    $properties = _node_type_flag_get_node_type_and_gid($entity_id, $this->group);
    $gid = $this->group ? $properties['gid'] : 0;

    $user_flags = node_type_flag_get_user_flags($properties['node_type'], $gid, $uid, $sid);
    return isset($user_flags[$this->name]) ? $user_flags[$this->name] : NULL;
  }

  /**
   * Similar to is_flagged() excepts it returns the flagging entity.
   */
  function get_flagging($entity_id, $uid = NULL, $sid = NULL) {
    if (($record = $this->get_flagging_record($entity_id, $uid, $sid))) {
      return node_type_flagging_load($record->flagging_id);
    }
  }

  /**
   * Determines if a certain user has flagged this object.
   *
   * You probably shouldn't call this raw private method: call the
   * is_flagged() method instead.
   *
   * This method is similar to is_flagged() except that it does direct SQL and
   * doesn't do caching. Use it when you want to not affect the cache, or to
   * bypass it.
   *
   * @return
   *   If the object is flagged, returns the value of the 'flagging_id' column.
   *   Else, returns FALSE.
   */
  function _is_flagged($entity_id, $uid, $sid) {
    $properties = _node_type_flag_get_node_type_and_gid($entity_id);
    $gid = $this->group ? $properties['gid'] : 0;

    return db_select('node_type_flagging', 'fc')
      ->fields('fc', array('flagging_id'))
      ->condition('fid', $this->fid)
      ->condition('uid', $uid)
      ->condition('sid', $sid)
      ->condition('node_type', $properties['node_type'])
      ->condition('gid', $gid)
       ->execute()
      ->fetchField();
  }

  /**
   * A low-level method to flag an object.
   *
   * You probably shouldn't call this raw private method: call the flag()
   * function instead.
   *
   * @return
   *   The 'flagging_id' column of the new {flagging} record.
   */
  function _flag($flagging) {
    $properties = _node_type_flag_get_node_type_and_gid($flagging->entity_id);
    $gid = $this->group ? $properties['gid'] : 0;

    $fields = array(
      'fid' => $this->fid,
      'node_type' => $properties['node_type'],
      'uid' => $flagging->uid,
      'sid' => $flagging->sid,
      'timestamp' => REQUEST_TIME,
      'gid' => $gid,
    );

    $flagging_id = db_insert('node_type_flagging')
      ->fields($fields)
      ->execute();
    return $flagging_id;
  }

  /**
   * A low-level method to unflag an object.
   *
   * You probably shouldn't call this raw private method: call the flag()
   * function instead.
   */
  function _unflag($entity_id, $flagging_id) {
    db_delete('node_type_flagging')
      ->condition('flagging_id', $flagging_id)
      ->execute();
  }

  /**
   * Increases the flag count for an object.
   *
   * @param $entity_id
   *   For which item should the count be increased.
   * @param $number
   *   The amount of counts to increasing. Defaults to 1.
  */
  function _increase_count($entity_id, $number = 1) {
    $properties = _node_type_flag_get_node_type_and_gid($entity_id);
    $gid = $this->group ? $properties['gid'] : 0;

    db_merge('node_type_flag_counts')
      ->key(array(
        'fid' => $this->fid,
        'node_type' => $properties['node_type'],
        'gid' => $gid,
      ))
      ->insertFields(array(
        'fid' => $this->fid,
        'node_type' => $properties['node_type'],
        'gid' => $gid,
        'count' => $number,
        'last_updated' => REQUEST_TIME,
      ))
      ->updateFields(array(
        'last_updated' => REQUEST_TIME,
      ))
      ->expression('count', 'count + :inc', array(':inc' => $number))
      ->execute();
  }

  /**
   * Decreases the flag count for an object.
   *
   * @param $entity_id
   *   For which item should the count be descreased.
   * @param $number
   *   The amount of counts to decrease. Defaults to 1.
  */
  function _decrease_count($entity_id, $number = 1) {
    $properties = _node_type_flag_get_node_type_and_gid($entity_id);
    $gid = $this->group ? $properties['gid'] : 0;

    // Delete rows with count 0, for data consistency and space-saving.
    // Done before the db_update() to prevent out-of-bounds errors on "count".
    db_delete('node_type_flag_counts')
      ->condition('fid', $this->fid)
      ->condition('node_type', $properties['node_type'])
      ->condition('gid', $gid)
      ->condition('count', $number, '<=')
      ->execute();

    // Update the count with the new value otherwise.
    db_update('node_type_flag_counts')
      ->expression('count', 'count - :inc', array(':inc' => $number))
      ->fields(array(
        'last_updated' => REQUEST_TIME,
      ))
      ->condition('node_type', $properties['node_type'])
      ->condition('gid', $gid)
      ->execute();
  }

  /**
   * Returns the number of times an item is flagged.
   *
   * Thanks to using a cache, inquiring several different flags about the same
   * item results in only one SQL query.
   */
  function get_count($entity_id) {
    $counts = node_type_flag_get_counts($this->entity_type, $entity_id);
    return isset($counts[$this->name]) ? $counts[$this->name] : 0;
  }

  /**
   * Returns the number of items a user has flagged.
   *
   * For global flags, pass '0' as the user ID and session ID.
   */
  function get_user_count($uid, $sid = NULL) {
    if (!isset($sid)) {
      $sid = flag_get_sid($uid);
    }
    return db_select('node_type_flagging', 'fc')->fields('fc', array('flagging_id'))
      ->condition('fid', $this->fid)
      ->condition('uid', $uid)
      ->condition('sid', $sid)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Deletes a flag from the database.
   */
  function delete() {
    db_delete('flag')->condition('fid', $this->fid)->execute();
    db_delete('node_type_flagging')->condition('fid', $this->fid)->execute();
    db_delete('flag_types')->condition('fid', $this->fid)->execute();
    db_delete('node_type_flag_counts')->condition('fid', $this->fid)->execute();
    module_invoke_all('flag_delete', $this);
  }

  /**
   * Returns token types for the current entity type.
   */
  function get_labels_token_types() {
    // The token type name might be different to the entity type name. If so,
    // an own flag entity handler can be used for overriding this.
    $entity_info = entity_get_info('node');
    if (isset($entity_info['token type'])) {
      return array_merge(array($entity_info['token type']), parent::get_labels_token_types());
    }
    else {
      return array_merge(array('node'), parent::get_labels_token_types());
    }
  }

  /**
   * Replaces tokens.
   */
  function replace_tokens($label, $contexts, $options, $entity_id) {
    if ($entity_id && ($entity = node_load($entity_id))) {
      $contexts['node'] = $entity;
    }
    return parent::replace_tokens($label, $contexts, $options, $entity_id);
  }

  /**
   * Almost identical to flag_flag:flag(). Needed for 3 reasons:
   * 1. Calls node_type_flagging_load() instead of flagging_load().
   * 2. Calls overriden private functions (which is not possible from
   * the parent).
   * 3. No hook_entity_presave() is called on the flagging object as it
   * is not an entity.
   */
  function flag($action, $entity_id, $account = NULL, $skip_permission_check = FALSE, $flagging = NULL) {
    // Get the user.
    if (!isset($account)) {
      $account = $GLOBALS['user'];
    }
    if (!$account) {
      return FALSE;
    }

    // Check access and applicability.
    if (!$skip_permission_check) {
      if (!$this->access($entity_id, $action, $account)) {
        $this->errors['access-denied'] = t('You are not allowed to flag, or unflag, this content.');
        // User has no permission to flag/unflag this object.
        return FALSE;
      }
    }
    else {
      // We are skipping permission checks. However, at a minimum we must make
      // sure the flag applies to this entity type:
      if (!$this->applies_to_entity_id($entity_id)) {
        $this->errors['entity-type'] = t('This flag does not apply to this entity type.');
        return FALSE;
      }
    }

    if (($this->errors = module_invoke_all('flag_validate', $action, $this, $entity_id, $account, $skip_permission_check, $flagging))) {
      return FALSE;
    }

    // Clear various caches; We don't want code running after us to report
    // wrong counts or false flaggings.
    drupal_static_reset('flag_get_counts');
    drupal_static_reset('flag_get_user_flags');
    drupal_static_reset('flag_get_entity_flags');

    // Find out which user id to use.
    $uid = $this->global ? 0 : $account->uid;

    // Find out which session id to use.
    if ($this->global) {
      $sid = 0;
    }
    else {
      $sid = flag_get_sid($uid, TRUE);
      // Anonymous users must always have a session id.
      if ($sid == 0 && $account->uid == 0) {
        $this->errors['session'] = t('Internal error: You are anonymous but you have no session ID.');
        return FALSE;
      }
    }

    // Set our uid and sid to the flagging object.
    if (isset($flagging)) {
      $flagging->uid = $uid;
      $flagging->sid = $sid;
    }

    // @todo: Discuss: Should we call field_attach_validate()? None of the
    // entities in core does this (fields entered through forms are already
    // validated).
    //
    // @todo: Discuss: Core wraps everything in a try { }, should we?

    // Perform the flagging or unflagging of this flag.
    $existing_flagging_id = $this->_is_flagged($entity_id, $uid, $sid);
    $flagged = (bool) $existing_flagging_id;
    if ($action == 'unflag') {
      if ($this->uses_anonymous_cookies()) {
        $this->_unflag_anonymous($entity_id);
      }
      if ($flagged) {
        if (!isset($flagging)) {
          $flagging = node_type_flagging_load($existing_flagging_id);
        }
        $transaction = db_transaction();
        try {
          // Note the order: We decrease the count first so hooks have accurate
          // data, then invoke hooks, then delete the flagging entity.
          $this->_decrease_count($entity_id);
          module_invoke_all('flag_unflag', $this, $entity_id, $account, $flagging);
          // Invoke Rules event.
          if (module_exists('rules')) {
            $event_name = 'flag_unflagged_' . $this->name;
            if (entity_get_info($this->entity_type)) {
              $variables = array(
                'flag' => $this,
                'flagged_node' => $entity_id,
                'flagging_user' => $account,
                'flagging' => $flagging,
              );
              rules_invoke_event_by_args($event_name, $variables);
            }
          }
          $this->_unflag($entity_id, $flagging->flagging_id);
        }
        catch (Exception $e) {
          $transaction->rollback();
          watchdog_exception('flag', $e);
          throw $e;
        }
      }
    }
    elseif ($action == 'flag') {
      if ($this->uses_anonymous_cookies()) {
        $this->_flag_anonymous($entity_id);
      }
      // By definition there is no flagging entity, but we may have been passed
      // one in to save.
      if (!isset($flagging)) {
        // Construct a new flagging object if we don't have one.
        $flagging = $this->new_flagging($entity_id, $uid, $sid);
      }
      // Invoke hook_entity_presave().
      if (!$flagged) {
        // The entity is unflagged.
        // Save the flagging entity (just our table).
        $flagging_id = $this->_flag($flagging);
        // The _flag() method is a plain DB record writer, so it's a bit
        // antiquated. We have to explicitly get our new ID out.
        $flagging->flagging_id = $flagging_id;

        $this->_increase_count($entity_id);
        module_invoke_all('flag_flag', $this, $entity_id, $account, $flagging);
        // Invoke Rules event.
        if (module_exists('rules')) {
          $event_name = 'flag_flagged_' . $this->name;
          if (entity_get_info('node')) {
            $variables = array(
              'flag' => $this,
              'flagged_node' => $entity_id,
              'flagging_user' => $account,
              'flagging' => $this->get_flagging($entity_id, $account->uid),
            );
            rules_invoke_event_by_args($event_name, $variables);
          }
        }
      }
    }

    return TRUE;
  }
}
